﻿using Postgres.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Postgres
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new DefaultDbContext())
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var newPerson1 = new Person { Name = "New", Surname = "Boy", Age = 25, DateCreated = DateTime.Now };
                    var newPerson2 = new Person { Name = "Other", Surname = "Girl", Age = 28, DateCreated = DateTime.Now };
                    context.Persons.Add(newPerson1);
                    context.Persons.Add(newPerson2);
                    context.SaveChanges();

                    var newCar1 = new Car { Brand = "Porsche", Model = "911", PersonId = newPerson1.Id };
                    var newCar2 = new Car { Brand = "Ferrari", Model = "488" };
                    var otherPerson = context.Persons.SingleOrDefault(x => x.Name == "Other");
                    otherPerson.Cars = new List<Car>();
                    otherPerson.Cars.Add(newCar2);
                    context.Cars.Add(newCar1);
                    context.Cars.Add(newCar2);
                    context.SaveChanges();

                    context.Cars.SingleOrDefault(x => x.Model == "911").DateReleased = new DateTime(1963, 1, 1);
                    context.Cars.SingleOrDefault(x => x.Model == "488").DateReleased = new DateTime(2015, 1, 1);
                    context.SaveChanges();

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Console.WriteLine(ex.Message);
                }

                var persons = context.Persons.ToList();
                Console.WriteLine($"We have {persons.Count} person(s).");

                var cars = context.Cars.ToList();
                Console.WriteLine($"We have {cars.Count} car(s).");

                var cars20s = context.Cars.Where(x => DbFunctions.TruncateTime(x.DateReleased).Value.Year >= 2000).ToList();
                Console.WriteLine($"We have {cars20s.Count} car(s) newer than 20s.");

                Console.ReadKey();
            }
        }
    }
}
