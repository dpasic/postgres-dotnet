﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Postgres.Models
{
    [Table("Car")]
    public class Car
    {
        public int Id { get; set; }

        public Person Person { get; set; }
        public int PersonId { get; set; }

        public string Brand { get; set; }
        public string Model { get; set; }
        public DateTime? DateReleased { get; set; }

        [NotMapped]
        public double CO2 { get; set; }
    }
}
